libvmdk (20240510-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

  [ Hilko Bengen ]
  * Fix watch file
  * New upstream version 20240510
  * Replace build-dependency pkg-config => pkgconf
  * Drop libbfio build-dependency; use vendored version
  * Rebase patches
  * Add python3-setuptools build-dependency (Closes: #1080657)
  * Update symbols file
  * copyright: Remove superfluous file entries (found by Lintian)

 -- Hilko Bengen <bengen@debian.org>  Sun, 01 Dec 2024 23:44:02 +0000

libvmdk (20200926-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Use cross-build compatible macro for finding pkg-config.

  [ Sébastien Delafond ]
  * Disable memory tests on riscv-64 (Closes: #978529)

 -- Sebastien Delafond <seb@debian.org>  Wed, 27 Jan 2021 10:03:25 +0100

libvmdk (20200926-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 20200926.
  * debian/rules: removed no longer needed variable DEB_LDFLAGS_MAINT_APPEND.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Sat, 10 Oct 2020 05:15:59 +0000

libvmdk (20200810-1) unstable; urgency=medium

  [ Samuel Henrique ]
  * Team upload.
  * Configure git-buildpackage for Debian.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Remove Section on libvmdk1 that duplicates source.

  [ Francisco Vilmar Cardoso Ruviaro ]
  * New upstream version 20200810.
  * Updated symbols file.
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added 'Rules-Requires-Root: no'.
      - Marked python3-libvmdk as Multi-Arch: same.
  * debian/copyright:
      - Added new rights.
      - Updated packaging copyright.
      - Updated upstream copyright years.
  * debian/libvmdk1.symbols: added 'Build-Depends-Package' field.
  * debian/tests/autopkgtest-pkg-python.conf: created to run Autopkgtest
    successfully.

 -- Francisco Vilmar Cardoso Ruviaro <francisco.ruviaro@riseup.net>  Sun, 30 Aug 2020 21:20:43 +0000

libvmdk (20181227-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Remove the Python 2 bindings. (Closes: #936936)

  [ Samuel Henrique ]
  * Add salsa-ci.yml

 -- Adrian Bunk <bunk@debian.org>  Sun, 19 Jan 2020 00:04:09 +0200

libvmdk (20181227-1) unstable; urgency=medium

  * Team upload

  [ Raphaël Hertzog ]
  * Update team maintainer address to Debian Security Tools
    <team+pkg-security@tracker.debian.org>
  * Update Vcs-Git and Vcs-Browser for the move to salsa.debian.org

  [ Hilko Bengen ]
  * New upstream version 20181227
  * Drop unneeded dh --parallel parameter
  * Replace dh_install --fail-missing
  * Bump Standards-Version
  * Update symbols
  * Enable hardening rules
  * Drop patch

 -- Hilko Bengen <bengen@debian.org>  Sun, 13 Jan 2019 23:05:43 +0100

libvmdk (20170226-3) unstable; urgency=medium

  * Fix typo in debian/control

 -- Hilko Bengen <bengen@debian.org>  Mon, 13 Nov 2017 13:32:35 +0100

libvmdk (20170226-2) unstable; urgency=medium

  * Modernize package: Fix Vcs URLs, remove -dbg package, bump DH compat
    level, Standards-Version

 -- Hilko Bengen <bengen@debian.org>  Mon, 13 Nov 2017 13:29:54 +0100

libvmdk (20170226-1) unstable; urgency=medium

  * New upstream version 20170226
  * Bump Standards-Version
  * Add missing files for tests
  * Update symbols
  * Disable Python tests

 -- Hilko Bengen <bengen@debian.org>  Tue, 28 Feb 2017 14:15:53 +0100

libvmdk (20160119-3) unstable; urgency=medium

  * Add Python 3 bindings

 -- Hilko Bengen <bengen@debian.org>  Fri, 18 Mar 2016 01:27:45 +0100

libvmdk (20160119-2) unstable; urgency=medium

  * Update symbols file
  * Bump libbfio-dev dependency

 -- Hilko Bengen <bengen@debian.org>  Sun, 24 Jan 2016 14:48:11 +0100

libvmdk (20160119-1) unstable; urgency=medium

  * New upstream version

 -- Hilko Bengen <bengen@debian.org>  Sun, 24 Jan 2016 11:47:20 +0100

libvmdk (20160108-1) unstable; urgency=medium

  * New upstream version
  * Don't mention multiarch-support directly in Pre-Depends line.

 -- Hilko Bengen <bengen@debian.org>  Thu, 14 Jan 2016 19:34:09 +0100

libvmdk (20150516-2) unstable; urgency=medium

  * Fixed watch file
  * Gave package to Debian Forensics team

 -- Hilko Bengen <bengen@debian.org>  Fri, 31 Jul 2015 19:58:02 +0200

libvmdk (20150516-1) unstable; urgency=low

  * Initial release (Closes: #792348)

 -- Hilko Bengen <bengen@debian.org>  Sun, 19 Jul 2015 21:44:21 +0200
