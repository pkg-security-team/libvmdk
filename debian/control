Source: libvmdk
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Hilko Bengen <bengen@debian.org>
Build-Depends: debhelper-compat (= 13),
 dh-python,
 pkgconf,
 libfuse-dev, zlib1g-dev,
 python3-dev, python3-setuptools,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Section: libs
Homepage: https://github.com/libyal/libvmdk
Vcs-Git: https://salsa.debian.org/pkg-security-team/libvmdk.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/libvmdk

Package: libvmdk-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
 libvmdk1 (= ${binary:Version})
Description: VMWare Virtual Disk format access library -- development files
 libvmdk is a library to access the VMware Virtual Disk (VMDK) format.
 .
 This package includes the development support files.

Package: libvmdk1
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: VMWare Virtual Disk format access library
 libvmdk is a library to access the VMware Virtual Disk (VMDK) format.
 .
 This package contains the shared library.

Package: libvmdk-utils
Section: otherosfs
Architecture: any
Multi-Arch: foreign
Depends: ${shlibs:Depends}, ${misc:Depends},
 libvmdk1,
Description: VMWare Virtual Disk format access library -- Utilities
 libvmdk is a library to access the VMware Virtual Disk (VMDK) format.
 .
 This package contains tools to access data stored in VMDK files:
 vmdkinfo, vmdkmount.

Package: python3-libvmdk
Section: python
Architecture: any
Multi-Arch: same
Depends: libvmdk1 (= ${binary:Version}), ${shlibs:Depends}, ${misc:Depends}, ${python3:Depends}
Description: VMWare Virtual Disk format access library -- Python 3 bindings
 libvmdk is a library to access the VMware Virtual Disk (VMDK) format.
 .
 This package contains Python 3 bindings for libvmdk.
